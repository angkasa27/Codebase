import React from 'react';
import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import Login from '../index';

jest.mock('../action');
jest.mock('../component');

describe('Login', () => {
  it('has connected redux props', () => {
    const mockStore = configureMockStore([thunk]);
    const initialState = {
      login: {},
      loading: {},
    };
    const store = mockStore(initialState);
    const wrapper = shallow(<Login store={store} />);

    expect(wrapper.props()).toHaveProperty('isLoading');
    expect(wrapper.props()).toHaveProperty('message');
  });
});
