import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Component from './component';
import * as actions from './action';
import styles from './styles';
import { openSnackbar } from '../../components/elements/Snackbar/action';
import * as sideDrawerActions from '../../components/elements/SideDrawer/action';

function mapStateToProps(state) {
  const { isLoading, data, meta, nik } = state.tribeZero;
  return {
    isLoading,
    data,
    meta,
    nik,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    openSnackbar: bindActionCreators(openSnackbar, dispatch),
    sideDrawerActions: bindActionCreators(sideDrawerActions, dispatch),
  };
}

const Styled = withStyles(styles)(Component);

export default connect(mapStateToProps, mapDispatchToProps)(Styled);
