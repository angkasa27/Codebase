import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Component from './component';
import { openSnackbar } from '../../components/elements/Snackbar/action';

function mapDispatchToProps(dispatch) {
  return {
    openSnackbar: bindActionCreators(openSnackbar, dispatch)
  };
}

export default connect(null, mapDispatchToProps)(Component);
