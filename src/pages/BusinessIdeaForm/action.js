/* eslint-disable max-lines */
import axios from 'axios';
import { customerList } from '../../utils/dropdownList';
import services from '../../configs/services';
import _ from 'lodash';

function HtmlToPlainText(value) {
  return value.replace(/<[^>]+>/g, '');
}

export async function handleFetchData(id) {
  return new Promise((resolve, reject) => {
    axios
      .get(`${services.GET_BUSINESS_IDEA(`/${id}`)}`)
      .then((result) => {
        const data = result.data.data;

        let initState = { id: data.id };

        // === Page 1
        if (data.name) initState = { ...initState, name: data.name };
        if (data.productType) initState = { ...initState, productType: data.productType };
        if (data.productName) initState = { ...initState, productName: data.productName };
        if (data.productLogo)
          initState = {
            ...initState,
            productLogoUrl: services.GET_BUSINESS_IDEA_LOGO(data.productLogo),
            productLogo: {
              ...initState.goldenCircle,
              isUploaded: true,
            },
          };
        if (data.type) initState = { ...initState, type: data.type };
        if (data.area) initState = { ...initState, area: data.area };
        if (data.businessField)
          initState = { ...initState, businessField: data.businessField.array };
        if (data.customer) {
          let findValue = _.find(customerList, function(item) {
            return item.value === result.data.data.customer;
          });

          if (!findValue) {
            initState = {
              ...initState,
              customer: 'Other',
              otherCustomer: data.customer,
            };
          } else {
            initState = {
              ...initState,
              customer: data.customer,
              otherCustomer: '',
            };
          }
        }

        // === Page 2
        if (data.background)
          initState = {
            ...initState,
            background: data.background,
            backgroundPlain: HtmlToPlainText(data.background),
          };
        if (data.desc)
          initState = {
            ...initState,
            desc: data.desc,
            descPlain: HtmlToPlainText(data.desc),
          };
        if (data.telkomBenefit)
          initState = {
            ...initState,
            telkomBenefit: data.telkomBenefit,
            telkomBenefitPlain: HtmlToPlainText(data.telkomBenefit),
          };
        if (data.customerBenefit)
          initState = {
            ...initState,
            customerBenefit: data.customerBenefit,
            customerBenefitPlain: HtmlToPlainText(data.customerBenefit),
          };
        if (data.valueCapture)
          initState = {
            ...initState,
            valueCapture: data.valueCapture,
            valueCapturePlain: HtmlToPlainText(data.valueCapture),
          };

        // === Page 3
        if (data.goldenCircleUrl)
          initState = {
            ...initState,
            goldenCircleUrl: services.GET_BUSINESS_IDEA_CANVAS(data.goldenCircleUrl),
            goldenCircle: {
              ...initState.goldenCircle,
              isUploaded: true,
            },
          };
        if (data.goldenCircleWhy)
          initState = { ...initState, goldenCircleWhy: data.goldenCircleWhy };
        if (data.goldenCircleHow)
          initState = { ...initState, goldenCircleHow: data.goldenCircleHow };
        if (data.goldenCircleWhat)
          initState = { ...initState, goldenCircleWhat: data.goldenCircleWhat };
        if (data.valuePropCustomerUrl)
          initState = {
            ...initState,
            valuePropCustomerUrl: services.GET_BUSINESS_IDEA_CANVAS(data.valuePropCustomerUrl),
            valuePropCustomer: {
              ...initState.valuePropCustomer,
              isUploaded: true,
            },
          };
        if (data.valuePropCustomerJobs)
          initState = {
            ...initState,
            valuePropCustomerJobs: data.valuePropCustomerJobs,
          };
        if (data.valuePropCustomerPain)
          initState = {
            ...initState,
            valuePropCustomerPain: data.valuePropCustomerPain,
          };
        if (data.valuePropCustomerGain)
          initState = {
            ...initState,
            valuePropCustomerGain: data.valuePropCustomerGain,
          };
        if (data.valuePropCustomerProductService)
          initState = {
            ...initState,
            valuePropCustomerProductService: data.valuePropCustomerProductService,
          };
        if (data.valuePropCustomerPainRelievers)
          initState = {
            ...initState,
            valuePropCustomerPainRelievers: data.valuePropCustomerPainRelievers,
          };
        if (data.valuePropCustomerGainCreators)
          initState = {
            ...initState,
            valuePropCustomerGainCreators: data.valuePropCustomerGainCreators,
          };
        if (data.leanCanvasUrl)
          initState = {
            ...initState,
            leanCanvasUrl: services.GET_BUSINESS_IDEA_CANVAS(data.leanCanvasUrl),
            leanCanvas: {
              ...initState.leanCanvas,
              isUploaded: true,
            },
          };
        if (data.leanCanvasDesc)
          initState = {
            ...initState,
            additionalCanvasDesc: data.leanCanvasDesc,
          };
        if (data.businessModelUrl)
          initState = {
            ...initState,
            businessModelUrl: services.GET_BUSINESS_IDEA_CANVAS(data.businessModelUrl),
            businessModel: {
              ...initState.businessModel,
              isUploaded: true,
            },
          };
        if (data.businessModelDesc)
          initState = {
            ...initState,
            additionalCanvasDesc: data.businessModelDesc,
          };

        if (data.additionalCanvasName)
          initState = {
            ...initState,
            additionalCanvasName: data.additionalCanvasName,
          };

        // === page 4
        if (data.omtm) initState = { ...initState, omtm: data.omtm };
        if (data.omtmDesc) initState = { ...initState, omtmDesc: data.omtmDesc };
        if (data.omtmTarget)
          initState = {
            ...initState,
            omtmTarget: data.omtmTarget,
            omtmTargetFormatted: data.omtmTarget.replace('.', ','),
          };
        if (data.execStart && data.execEnd)
          initState = {
            ...initState,
            execDate: { startDate: data.execStart, endDate: data.execEnd },
          };

        // === page 5

        let talentData = [],
          squadData = [];

        if (data.squadRequest) {
          data.squadRequest.array.map((squad) => {
            talentData = [];

            squad.talent_request.map((talent) => {
              talentData.push({
                jobRole: talent.job_role || '',
                jobLevel: talent.job_level || '',
                amountSubmitted: talent.submitted || '',
                amountExisting: talent.existing || talent.existing === 0 ? talent.existing : '',
                talentExisting: talent.existing_talent || [],
              });
            });

            squadData.push({
              squadName: squad.squad_name || '',
              talentRequest: talentData,
            });
          });

          initState = { ...initState, squadRequest: squadData };
        }

        if (data.costTalent) initState = { ...initState, costTalent: parseInt(data.costTalent) };
        if (data.costEnabler) initState = { ...initState, costEnabler: parseInt(data.costEnabler) };
        if (data.rewardForTalent)
          initState = {
            ...initState,
            rewardForTalent: parseInt(data.rewardForTalent),
          };
        if (data.development) initState = { ...initState, development: parseInt(data.development) };
        if (data.partnership) initState = { ...initState, partnership: parseInt(data.partnership) };
        if (data.operational) initState = { ...initState, operational: parseInt(data.operational) };

        resolve(initState);
      })

      .catch((e) => {
        reject(e);
      });
  });
}

export function handleSendForm(data, isComplete = false) {
  return new Promise((resolve, reject) => {
    // submitForm
    let sendData = {};

    if (data.name && data.name !== '') sendData = { ...sendData, name: data.name };
    if (data.productType && data.productType !== '')
      sendData = { ...sendData, product_type: data.productType };
    if (data.productName && data.productName !== '')
      sendData = { ...sendData, product_name: data.productName };
    if (data.type && data.type !== '') sendData = { ...sendData, type: data.type };
    if (data.area && data.area !== '') sendData = { ...sendData, area: data.area };
    if (data.businessField && data.businessField.length > 0)
      sendData = { ...sendData, business_field: data.businessField };
    if (data.customer && data.customer !== '') {
      let customer = data.customer !== 'Other' ? data.customer : data.otherCustomer;
      sendData = { ...sendData, customer: customer };
    }
    if (data.background && data.background !== '')
      sendData = { ...sendData, background: data.background };
    if (data.desc && data.desc !== '') sendData = { ...sendData, desc: data.desc };
    if (data.telkomBenefit && data.telkomBenefit !== '')
      sendData = { ...sendData, telkom_benefit: data.telkomBenefit };
    if (data.customerBenefit && data.customerBenefit !== '')
      sendData = { ...sendData, customer_benefit: data.customerBenefit };
    if (data.valueCapture && data.valueCapture !== '')
      sendData = { ...sendData, value_capture: data.valueCapture };
    if (
      (data.goldenCircleWhy && data.goldenCircleWhy !== '') ||
      (data.goldenCircleHow && data.goldenCircleHow !== '') ||
      (data.goldenCircleWhat && data.goldenCircleWhat !== '')
    ) {
      sendData = {
        ...sendData,
        golden_circle_desc: {
          why: data.goldenCircleWhy || '',
          how: data.goldenCircleHow || '',
          what: data.goldenCircleWhat || '',
        },
      };
    }

    if (
      (data.valuePropCustomerJobs && data.valuePropCustomerJobs !== '') ||
      (data.valuePropCustomerPain && data.valuePropCustomerPain !== '') ||
      (data.valuePropCustomerGain && data.valuePropCustomerGain !== '') ||
      (data.valuePropCustomerProductService && data.valuePropCustomerProductService !== '') ||
      (data.valuePropCustomerPainRelievers && data.valuePropCustomerPainRelievers !== '') ||
      (data.valuePropCustomerGainCreators && data.valuePropCustomerGainCreators !== '')
    ) {
      sendData = {
        ...sendData,
        value_prop_desc: {
          job: data.valuePropCustomerJobs || '',
          pain: data.valuePropCustomerPain || '',
          gain: data.valuePropCustomerGain || '',
          products_services: data.valuePropCustomerProductService || '',
          pain_relievers: data.valuePropCustomerPainRelievers || '',
          gain_creators: data.valuePropCustomerGainCreators || '',
        },
      };
    }

    if (data.additionalCanvasName !== '') {
      sendData = { ...sendData, additional_canvas_name: data.additionalCanvasName };
      if (isComplete) {
        if (data.additionalCanvasName === 'Lean Canvas') {
          sendData = {
            ...sendData,
            lean_canvas_desc: {
              desc:
                data.additionalCanvasDesc !== '' && data.additionalCanvasName === 'Lean Canvas'
                  ? data.additionalCanvasDesc
                  : '',
            },
          };
        } else {
          sendData = {
            ...sendData,
            business_model_canvas_desc: {
              desc:
                data.additionalCanvasDesc !== '' &&
                data.additionalCanvasName === 'Business Model Canvas'
                  ? data.additionalCanvasDesc
                  : '',
            },
          };
        }
      } else {
        sendData = {
          ...sendData,
          lean_canvas_desc: {
            desc:
              data.additionalCanvasDesc !== '' && data.additionalCanvasName === 'Lean Canvas'
                ? data.additionalCanvasDesc
                : '',
          },
          business_model_canvas_desc: {
            desc:
              data.additionalCanvasDesc !== '' &&
              data.additionalCanvasName === 'Business Model Canvas'
                ? data.additionalCanvasDesc
                : '',
          },
        };
      }
    }

    if (
      (data.omtm && data.omtm !== '') ||
      (data.omtmDesc && data.omtmDesc !== '') ||
      data.omtmTarget ||
      data.omtmTarget !== ''
    ) {
      sendData = {
        ...sendData,
        omtm: {
          name: data.omtm || '',
          desc: data.omtmDesc || '',
          target: parseFloat(data.omtmTarget) || '',
        },
      };
    }

    if (data.execDate.startDate) {
      sendData = {
        ...sendData,
        exec_start: data.execDate.startDate,
        exec_end: data.execDate.endDate,
      };
    }

    if (data.squadRequest) {
      let squadRequest = [],
        talentRequest = [];

      data.squadRequest.map((squad, idxSquad) => {
        squadRequest.push({
          squad_name: squad.squadName,
        });

        talentRequest = [];

        squad.talentRequest.map((talent) => {
          talentRequest.push({
            job_role: talent.jobRole,
            job_level: talent.jobLevel,
            submitted: parseInt(talent.amountSubmitted) || undefined,
            existing: parseInt(talent.amountExisting) || undefined,
            existing_talent: talent.talentExisting,
          });
        });

        if (talentRequest.length > 0) {
          squadRequest[idxSquad] = {
            ...squadRequest[idxSquad],
            talent_request: talentRequest,
          };
        }
      });

      if (squadRequest.length > 0) {
        sendData = {
          ...sendData,
          squad_request: squadRequest,
        };
      }
    }

    if (data.costTalent !== '' && !isNaN(data.costTalent))
      sendData = { ...sendData, cost_talent: data.costTalent };

    if (data.costEnabler !== '' && !isNaN(data.costEnabler))
      sendData = { ...sendData, cost_enabler: data.costEnabler };

    if (data.rewardForTalent !== '' && !isNaN(data.rewardForTalent))
      sendData = { ...sendData, reward_for_talent: data.rewardForTalent };

    if (data.development !== '' && !isNaN(data.development))
      sendData = { ...sendData, development: data.development };

    if (data.partnership !== '' && !isNaN(data.partnership))
      sendData = { ...sendData, partnership: data.partnership };

    if (data.operational !== '' && !isNaN(data.operational))
      sendData = { ...sendData, operational: data.operational };

    if (data.additionalCanvasName)
      sendData = { ...sendData, additional_canvas_name: data.additionalCanvasName };

    const status = isComplete ? 'save' : 'draft';

    axios
      .post(services.SAVE_BUSINESS_IDEA(data.id, status), sendData)
      .then(() => {
        resolve(true);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

export function handleSendImage(data, progressFunc) {
  return new Promise((resolve, reject) => {
    let formData = new FormData();
    if (data.name !== 'productLogoUrl') formData.append('name', data.name);
    formData.append('image', data.files);

    const uploadUrl =
      data.name === 'productLogoUrl'
        ? services.SAVE_BUSINESS_IDEA_LOGO(data.id)
        : services.SAVE_BUSINESS_IDEA_CANVAS(data.id);

    axios
      .post(uploadUrl, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        onUploadProgress: function(progressEvent) {
          progressFunc(progressEvent);
        }.bind(this),
      })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        if (err.message === 'Network Error') {
          return resolve(err.message);
        }

        reject(err);
      });
  });
}

export function handleDeleteImage(data) {
  return new Promise((resolve, reject) => {
    const deleteUrl =
      data.name === 'productLogo'
        ? services.DELETE_BUSINESS_IDEA_LOGO(data.id)
        : services.DELETE_BUSINESS_IDEA_CANVAS(data.id);

    axios
      .delete(deleteUrl)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
