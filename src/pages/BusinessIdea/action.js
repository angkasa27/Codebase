import fetch from '../../utils/fetch';
import { SERVICES } from '../../configs';
import { ACTIONS } from '../../constants';
import { push } from 'react-router-redux';
import moment from 'moment';
import queryString from 'query-string';

export function setQuery(item) {
  if (item.isDate) {
    return (dispatch) => {
      const value = queryString.parse(location.search);
      value.startDate = item.from.toISOString();
      value.endDate = item.to.toISOString();
      const urlParser = `?${queryString.stringify(value)}`;
      dispatch(push(urlParser));
    };
  } else {
    return (dispatch) => {
      const value = queryString.parse(location.search);
      value[item.name] = item.value;
      const urlParser = `?${queryString.stringify(value)}`;
      dispatch(push(urlParser));
    };
  }
}

export function getListBusinessIdea() {
  return (dispatch) => {
    dispatch({ type: ACTIONS.LOADING });

    return new Promise((resolve, reject) => {
      const value = queryString.parse(location.search);
      const urlParser = `?${queryString.stringify(value)}`;

      fetch({
        method: 'GET',
        url: SERVICES.GET_BUSINESS_IDEA(urlParser),
      })
        .then((res) => {
          const transformedData = [];
          res.data.forEach((el) => {
            let status = 'Draft';
            switch (el.status) {
              case 1:
                status = 'Evaluated by Tribe Zero';
                break;
              case 2:
                status = 'Waiting for pitching schedule';
                break;
              case 3:
                status = 'Need revision';
                break;
              case 4:
                status = 'Pitching';
                break;
              case 5:
                status = 'Reject';
                break;
              case 6:
                status = 'On boarding';
                break;
              case 7:
                status = 'Running';
                break;
              case 8:
                status = 'Terminate';
                break;

              default:
                break;
            }

            transformedData.push({
              ...el,
              status: status,
              status_code: el.status,
              updated_at: moment(el.updated_at).format('DD/MM/YYYY, HH:mm'),
            });
          });
          dispatch({
            type: ACTIONS.LIST_OF_BUSINESS_IDEA_FETCHED,
            data: {
              data: transformedData,
              meta: {
                totalPage: res.meta.total_pages,
                page: res.meta.current_page,
                totalData: res.meta.total_data,
              },
            },
          });
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };
}

export function postBusinessIdea(nik) {
  return (dispatch) => {
    dispatch({ type: ACTIONS.LOADING });

    return new Promise((resolve, reject) => {
      fetch({
        method: 'POST',
        url: SERVICES.POST_BUSINESS_IDEA,
        data: {
          nik: nik,
        },
      })
        .then((res) => {
          dispatch({ type: ACTIONS.POST_BUSINESS_IDEA });

          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };
}

export function downloadBusinessIdea(id) {
  return new Promise((resolve, reject) => {
    fetch({
      method: 'GET',
      url: SERVICES.GET_BUSINESS_IDEA_PDF(id),
    })
      .then(() => {
        window.open(SERVICES.GET_BUSINESS_IDEA_PDF(id), '_blank');
        resolve(true);
      })
      .catch(() => {
        setTimeout(() => {
          fetch({
            method: 'GET',
            url: SERVICES.GET_BUSINESS_IDEA_PDF(id),
          })
            .then(() => {
              window.open(SERVICES.GET_BUSINESS_IDEA_PDF(id), '_blank');
              resolve(true);
            })
            .catch((err) => reject(err));
        }, 500);
      });
  });
}

export function deleteBusinessIdea(id) {
  return (dispatch) => {
    dispatch({ type: ACTIONS.LOADING });

    return new Promise((resolve, reject) => {
      fetch({
        method: 'DELETE',
        url: SERVICES.DELETE_BUSINESS_IDEA(id),
      })
        .then((res) => {
          dispatch({ type: ACTIONS.DELETE_BUSINESS_IDEA });

          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };
}
