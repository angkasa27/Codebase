import { getIn } from 'formik';

export default function TalentValidation(values, errors, touched) {
  for (let i = 0; i < values.length; i++) {
    if (
      getIn(errors, `squadRequest[${i}].squadName`) &&
      getIn(touched, `squadRequest[${i}].squadName`)
    ) {
      return true;
    }

    for (let j = 0; j < values[i].talentRequest.length; j++) {
      if (
        (getIn(errors, `squadRequest[${i}].talentRequest[${j}].jobRole`) &&
          getIn(touched, `squadRequest[${i}].talentRequest[${j}].jobRole`)) ||
        (getIn(errors, `squadRequest[${i}].talentRequest[${j}].jobLevel`) &&
          getIn(touched, `squadRequest[${i}].talentRequest[${j}].jobLevel`)) ||
        (getIn(errors, `squadRequest[${i}].talentRequest[${j}].amountSubmitted`) &&
          getIn(touched, `squadRequest[${i}].talentRequest[${j}].amountSubmitted`)) ||
        (getIn(errors, `squadRequest[${i}].talentRequest[${j}].amountExisting`) &&
          getIn(touched, `squadRequest[${i}].talentRequest[${j}].amountExisting`))
      ) {
        return true;
      }
      for (let k = 0; k < values[i].talentRequest[j].talentExisting.length; k++) {
        if (
          (getIn(errors, `squadRequest[${i}].talentRequest[${j}].talentExisting[${k}].name`) &&
            getIn(touched, `squadRequest[${i}].talentRequest[${j}].talentExisting[${k}].name`)) ||
          (getIn(errors, `squadRequest[${i}].talentRequest[${j}].talentExisting[${k}].email`) &&
            getIn(touched, `squadRequest[${i}].talentRequest[${j}].talentExisting[${k}].email`))
        ) {
          return true;
        }
      }
    }
  }
  return false;
}
