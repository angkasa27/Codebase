const AUTHENTICATION_BASE_URL_DEV =
  'http://cdp-backend-api-dev.vsan-apps.playcourt.id/api/users/v1';
const AUTHENTICATION_BASE_URL_PROD =
  'http://cdp-backend-api-dev.vsan-apps.playcourt.id/api/users/v1';
const BASE_URL_DEV = 'http://cdp-backend-api-dev.vsan-apps.playcourt.id/api';
const BASE_URL_PROD = 'http://cdp-backend-api-dev.vsan-apps.playcourt.id/api';
const MOCKUP_URL =
  'https://virtserver.swaggerhub.com/telkomdds/Dashboard_Performance_System/2.0.0-oas3/api';
const BASE_MOCKUP_URL =
  'https://virtserver.swaggerhub.com/telkomdds/Dashboard_Performance_System/1.0.0/api/v1';
// PMS
const BASE_URL_PMS_SOCKET = 'http://business-journey-dev.vsan-apps.playcourt.id';
const BASE_URL_PMS_DEV = 'http://business-journey-dev.vsan-apps.playcourt.id/api/v1';
const BASE_URL_PMS_PROD = 'http://business-journey-dev.vsan-apps.playcourt.id/api/v1';
const BASE_URL_PMS_UAT = 'http://business-journey-dev.vsan-apps.playcourt.id/api/v1';

const BASE_URL_TMS_DEV = 'http://tms-api-dev.vsan-apps.playcourt.id/api/v1';
const BASE_URL_TMS_PROD = 'http://tms-api-dev.vsan-apps.playcourt.id/api/v1';

let BASE_URL = process.env.NODE_ENV === 'production' && !__DEV__ ? BASE_URL_PROD : BASE_URL_DEV;
if (typeof __UAT__ !== 'undefined' && __UAT__) BASE_URL = BASE_URL_UAT;

let BASE_URL_PMS =
  process.env.NODE_ENV === 'production' && !__DEV__ ? BASE_URL_PMS_PROD : BASE_URL_PMS_DEV;
if (typeof __UAT__ !== 'undefined' && __UAT__) BASE_URL = BASE_URL_PMS_UAT;

let BASE_URL_TMS =
  process.env.NODE_ENV === 'production' && !__DEV__ ? BASE_URL_TMS_PROD : BASE_URL_TMS_DEV;
if (typeof __UAT__ !== 'undefined' && __UAT__) BASE_URL = BASE_URL_TMS_UAT;

let AUTHENTICATION_BASE_URL =
  process.env.NODE_ENV === 'production' && !__DEV__
    ? AUTHENTICATION_BASE_URL_PROD
    : AUTHENTICATION_BASE_URL_DEV;
if (typeof __UAT__ !== 'undefined' && __UAT__) BASE_URL = BASE_URL_UAT;

const services = {
  GET_PROJECT_DETAILS: `${BASE_URL}/projects/v1`,
  GET_TALENTS_BY_PROJECT: `${BASE_URL}/projects/v1/talent`,
  GET_ACHIEVEMENT_BY_PROJECT: `${BASE_URL}/projects/v1/okr-achievement`,
  GET_SUMMARY_BY_PROJECT: `${BASE_URL}/projects/v1/summary`,
  GET_BUSINESS_IDEA: (params) => `${BASE_URL_PMS}/user/proposal${params}`,
  SAVE_BUSINESS_IDEA: (id, status) => `${BASE_URL_PMS}/user/proposal/${status}/${id}`,
  SAVE_BUSINESS_IDEA_CANVAS: (id) => `${BASE_URL_PMS}/user/image/canvas/${id}`,
  DELETE_BUSINESS_IDEA_CANVAS: (id) => `${BASE_URL_PMS}/user/image/canvas/${id}`,
  GET_BUSINESS_IDEA_CANVAS: (id) => `${BASE_URL_PMS}/user/image/canvas/${id}`,
  SAVE_BUSINESS_IDEA_LOGO: (id) => `${BASE_URL_PMS}/user/image/logo/${id}`,
  DELETE_BUSINESS_IDEA_LOGO: (id) => `${BASE_URL_PMS}/user/image/logo/${id}`,
  GET_BUSINESS_IDEA_LOGO: (id) => `${BASE_URL_PMS}/user/image/logo/${id}`,
  GET_BUSINESS_IDEA_PDF: (id) => `${BASE_URL_PMS}/user/proposal/pdf/${id}`,
  POST_BUSINESS_IDEA: `${BASE_URL_PMS}/user/proposal/add`,
  DELETE_BUSINESS_IDEA: (id) => `${BASE_URL_PMS}/user/proposal/${id}`,
  GET_TRIBE_ZERO: (params) => `${BASE_URL_PMS}/tribe-zero/proposal${params}`,
  SAVE_TRIBE_ZERO: (id, status) => `${BASE_URL_PMS}/tribe-zero/proposal/comment/${status}/${id}`,
  EDIT_PITCHING_SCHEDULE: (id) => `${BASE_URL_PMS}/tribe-zero/pitching-schedule/${id}`,
  GET_TRIBE_ZERO_PDF: (id) => `${BASE_URL_PMS}/tribe-zero/proposal/pdf/${id}`,
  GET_LIST_TRIBE: `${BASE_URL_PMS}/tribe`,
  ADD_TRIBE: `${BASE_URL_PMS}/tribe`,
  GET_LIST_PITCHING_ADDRESS: `${BASE_URL_PMS}/tribe/pitching`,
  ADD_PITCHING_ADDRESS: `${BASE_URL_PMS}/tribe/pitching`,
  EDIT_PITCHING_ADDRESS: (id) => `${BASE_URL_PMS}/tribe/pitching/${id}`,
  DELETE_PITCHING_ADDRESS: (id) => `${BASE_URL_PMS}/tribe/pitching/${id}`,
  GET_LIST_JOB_ROLE: `${BASE_URL_TMS}/jobRoles`,
  BASE_URL_PMS,
  BASE_URL_PMS_SOCKET,
  LOGIN: `${AUTHENTICATION_BASE_URL}`,
  GET_LIST_PRODUCT_FILTER: `${BASE_URL}/products/v1`,
  GET_LIST_PRODUCT_SUMMARY: `${BASE_URL}/products/v1/summary`,
  GET_LIST_TRIBE_FILTER: `${BASE_URL}/tribes/v1`,
};

export default services;
