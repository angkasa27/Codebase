import { reduxForm } from 'redux-form';
import Component from './component';
const Styled = (Component);

export default reduxForm({
  form: 'login',
})(Styled);
