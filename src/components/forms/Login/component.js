import React from 'react';
import { Field } from 'redux-form';
import Checkbox from '@material-ui/core/Checkbox';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import CircleCheckedFilled from '@material-ui/icons/CheckCircle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import PropTypes from 'prop-types';
import Button from '../../elements/Button';
import { ICONS } from '../../../configs';
import styles from './styles.css';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    const remember = localStorage.getItem('rememberMe');
    const nik = localStorage.getItem('nik');

    this.state = {
      nik: remember === 'true' ? nik : '',
      rememberMe: remember === 'true' ? true : false,
      showPassword: false,
    };
  }

  componentDidMount() {
    const { nik } = this.state;
    
    this.props.initialize({
      nik: nik,
    });
  }


  _handleChange = (event) => {
    const input = event.target;
    const value = input.type === 'checkbox' ? input.checked : input.value;
    this.setState({ [input.name]: value });
  };

  _handleCheckedSubmit = () => {
    const { nik, rememberMe } = this.state;

    localStorage.setItem('rememberMe', !rememberMe);
    localStorage.setItem('nik', nik);
  }

  _handleClickShowPassword = () => {
    const { showPassword } = this.state;

    this.setState({ showPassword: !showPassword });
  }

  render() {
    const {
      code,
      handleSubmit,
    } = this.props;
    const { showPassword } = this.state;
    const customClassUsername = code ? styles['red-textfield'] : styles['textfield'];
    const customClassPassword = code && code === 401 ? styles['red-textfield'] : styles['textfield'];    

    return (
      <form className={styles.form} onSubmit={handleSubmit}>
        <section>
          <header className={styles['header-input']}>
            <h4>NIK</h4>
          </header>
          <Field
            className={customClassUsername}
            component="input"
            name="nik"
            onChange={this._handleChange}
            placeholder="Type your portal NIK here"
            required
            type="number"
          />
        </section>
        <section>
          <header className={`${styles['header-input']} ${styles['header-input-password']}`}>
            <h4>Password</h4>
          </header>
          <div className={customClassPassword}>
            <Field
              className={styles['password-input']}
              component="input"
              name="password"
              placeholder="Type your portal password"
              required
              type={showPassword ? 'text' : 'password'}
            />
            <img className={styles['show-password']} onClick={this._handleClickShowPassword} src={showPassword ? ICONS.ICON_EYE : ICONS.ICON_EYE_HIDE} />
          </div>
        </section>
        <div>
          <FormControlLabel
            className={styles.checkBox}
            control={<Checkbox
              checked={this.state.rememberMe}
              checkedIcon={<CircleCheckedFilled 
                style={{ color: '#289f97' }} />}
              icon={<CircleUnchecked />}
              iconStyle={{ fill: '#289f97' }}
              inputStyle={{ color: '#289f97' }}
              labelStyle={{ color: '#289f97' }}
              name="rememberMe"
              onChange={this._handleChange}
              onClick={this._handleCheckedSubmit}
              style={{ color: '#707a89' }} />
            }
            label="Remember Me"
            labelPlacement="end"
          />
        </div>
        <Button large type="submit">Sign In</Button>
      </form>
    );
  }
}

Component.defaultProps = {
  code: 0,
  handleSubmit: '',
  initialize: '',
  invalid: false,
  submitting: false,
};

Component.propTypes = {
  code: PropTypes.number,
  handleSubmit: PropTypes.func,
  initialize: PropTypes.func,
  invalid: PropTypes.bool,
  submitting: PropTypes.bool,
};
