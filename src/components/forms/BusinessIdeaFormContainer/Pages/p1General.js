import React from 'react';
import Grid from '@material-ui/core/Grid';
import {
  areaList,
  typeList,
  customerList,
  businessFieldList,
  productTypeList,
} from '../../../../utils/dropdownList';
import { placeholder } from '../../../../constants/copywriting';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldSelect from '../../../elements/FieldSelect';
import FieldText from '../../../elements/FieldText';
import Dropzone from '../../../elements/Dropzone';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      values,
      errors,
      touched,
      setFieldValue,
      setFieldTouched,
      handleAutosave,
      onFilesAdded,
      onFilesRemoved,
    } = this.props;

    return (
      <>
        <h3>General and Background</h3>
        <p className="sub-text">
          This page identify general information about business idea proposals to be submitted.
        </p>
        {/* Business Name Field */}
        <Field
          name="name"
          render={({ field }) => (
            <FieldText
              {...field}
              data-cy="idea-name"
              error={errors.name}
              label="Name of Business Idea"
              long
              onChange={async (e) => {
                setFieldTouched('name', true);
                await setFieldValue('name', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.shortText('name of business idea')}
              required
              touched={touched.name}
              type="text"
            />
          )}
        />

        <Grid className="field-group" container>
          <Field
            name="productType"
            render={({ field }) => (
              <FieldSelect
                {...field}
                dataCy="product-type"
                error={errors.productType}
                label="Product"
                long
                onBlur={() => setFieldTouched('productType', true)}
                onChange={(e) => {
                  setFieldValue('productName', '');
                  setFieldTouched('productName', false, false);
                  setFieldValue('productType', e);
                  if (e === 3) setFieldValue('productName', '-');
                  handleAutosave;
                }}
                options={productTypeList}
                placeholder={placeholder.select('product')}
                required
                touched={touched.productType}
              />
            )}
            type="select"
          />
          {values.productType === 1 || values.productType === 2 ? (
            <Field
              name="productName"
              render={({ field }) => (
                <FieldText
                  {...field}
                  data-cy="product-name"
                  error={errors.productName}
                  long
                  onChange={(e) => {
                    setFieldTouched('productName', true);
                    setFieldValue('productName', e.target.value);
                    handleAutosave;
                  }}
                  placeholder={
                    values.productType === 1
                      ? placeholder.shortText('name of existing product')
                      : placeholder.shortText('name of new product')
                  }
                  required
                  touched={touched.productName}
                  type="text"
                />
              )}
            />
          ) : null}
        </Grid>
        <Grid container style={{ marginBottom: '1rem' }}>
          <Grid item lg={6} md={6} xs={12}>
            <Dropzone
              acceptFormat="image/jpeg,image/png,image/svg+xml"
              dataCy="product-logo"
              error={errors.productLogoUrl}
              handleTouch={setFieldTouched}
              image={values.productLogoUrl}
              initialValues={{
                bodyText: 'Click or drag image here (format jpg/png/svg)',
                captionText: '30 MB Maximum',
              }}
              isFailed={values.productLogo.isFailed}
              isUploaded={values.productLogo.isUploaded}
              label="Logo of Product"
              name="productLogoUrl"
              onFilesAdded={(files) => onFilesAdded(files, 'productLogoUrl', setFieldValue, values)}
              onFilesRemoved={(files) =>
                onFilesRemoved(files, 'productLogoUrl', setFieldValue, values)
              }
              progress={values.productLogo.progress}
              style={{ width: '26rem' }}
              touched={touched.productLogoUrl}
            />
          </Grid>
        </Grid>

        <Field
          name="type"
          render={({ field }) => (
            <FieldSelect
              {...field}
              dataCy="business-type"
              error={errors.type}
              label="Type of Business Idea"
              long
              onBlur={() => setFieldTouched('type', true)}
              onChange={(e) => {
                setFieldValue('type', e);
                handleAutosave;
              }}
              options={typeList}
              placeholder={placeholder.select('type of business idea')}
              required
              touched={touched.type}
            />
          )}
          type="select"
        />

        <Field
          name="area"
          render={({ field }) => (
            <FieldSelect
              {...field}
              dataCy="business-area"
              error={errors.area}
              label="Area of Business Idea"
              long
              onBlur={() => setFieldTouched('area', true)}
              onChange={(e) => {
                setFieldValue('area', e);
                handleAutosave;
              }}
              options={areaList}
              placeholder={placeholder.select('area of business idea')}
              required
              touched={touched.area}
            />
          )}
          type="select"
        />

        <Field
          name="businessField"
          render={({ field }) => (
            <FieldSelect
              {...field}
              creatable
              dataCy="business-field"
              error={errors.businessField}
              label="Field of Business Idea"
              long
              multiple
              onBlur={() => setFieldTouched('businessField', true)}
              onChange={async (e) => {
                await setFieldValue('businessField', e);
                handleAutosave;
              }}
              options={businessFieldList}
              placeholder={placeholder.select('field of business idea')}
              required
              touched={touched.businessField}
              withSearch
            />
          )}
          type="select"
        />

        <Grid className="field-group" container>
          <Field
            name="customer"
            render={({ field }) => (
              <FieldSelect
                {...field}
                dataCy="business-customer"
                error={errors.customer}
                label="Customer of Business Idea"
                long
                onBlur={() => setFieldTouched('customer', true)}
                onChange={(e) => {
                  setFieldValue('customer', e);
                  handleAutosave;
                }}
                options={customerList}
                placeholder={placeholder.select('customer of business idea')}
                required
                touched={touched.customer}
              />
            )}
            type="select"
          />
          {values.customer === 'Other' ? (
            <Field
              name="otherCustomer"
              render={({ field }) => (
                <FieldText
                  {...field}
                  data-cy="business-customer-other"
                  error={errors.otherCustomer}
                  long
                  onChange={(e) => {
                    setFieldTouched('otherCustomer', true);
                    setFieldValue('otherCustomer', e.target.value);
                    handleAutosave;
                  }}
                  placeholder={placeholder.shortText('other customer of business idea')}
                  required
                  touched={touched.otherCustomer}
                  type="text"
                />
              )}
            />
          ) : null}
        </Grid>
      </>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  onFilesAdded: PropTypes.func.isRequired,
  onFilesRemoved: PropTypes.func.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
