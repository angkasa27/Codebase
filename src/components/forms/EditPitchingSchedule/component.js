import React from 'react';
import { Field } from 'formik';
import FieldText from '../../elements/FieldText';
import FieldDate from '../../elements/FieldDate';
import FieldTime from '../../elements/FieldTime';
import Button from '../../elements/Button';
import PropTypes from 'prop-types';
import SideDrawer from '../../elements/SideDrawer';
import Grid from '@material-ui/core/Grid';
import { placeholder } from '../../../constants/copywriting';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '../../elements/Dialog';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
    this._handleToggleDialog = this._handleToggleDialog.bind(this);
    this._handleOnSubmit = this._handleOnSubmit.bind(this);
  }

  _handleOnSubmit(e) {
    e.preventDefault();
    const { values, onRescheduleItem } = this.props;

    onRescheduleItem(values).then(() => {
      this.setState({
        isOpen: !this.state.isOpen,
      });
    });
  }

  _handleToggleDialog(e) {
    e.preventDefault();
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }
  render() {
    const { touched, errors, values, setFieldValue, setFieldTouched } = this.props;

    const disabled = Object.keys(errors).length > 0;

    return (
      <>
        <Dialog
          aria-describedby="alert-dialog-description"
          aria-labelledby="alert-dialog-title"
          onClose={this._handleToggleDialog}
          open={this.state.isOpen}
        >
          <DialogTitle id="alert-dialog-title">
            Are you sure that you want to reschedule the pitching schedule?
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              By submitting this change, the proposal evaluation form will be sent again to Digital
              Investment Committee.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button ghost onClick={this._handleToggleDialog} type="button">
              Cancel
            </Button>
            <Button autoFocus onClick={this._handleOnSubmit} type="button">
              Submit
            </Button>
          </DialogActions>
        </Dialog>
        <SideDrawer>
          <form method="POST">
            <h3 style={{ marginBottom: '2.5rem' }}>Edit Pitching Schedule</h3>
            <Field
              name="pitchingDate"
              render={({ field }) => (
                <FieldDate
                  {...field}
                  data-cy="exec-date"
                  dateFormat="DD/MM/YYYY"
                  error={errors.pitchingDate}
                  label="Pitching Schedule"
                  onChange={(e) => {
                    setFieldValue('pitchingDate', e);
                  }}
                  placeholder="DD/MM/YYYY"
                  required
                  touched={touched.pitchingDate}
                  useFormattedValue
                />
              )}
            />
            <Grid className="field-group" container>
              <Field
                name="pitchingTimeStart"
                render={({ field }) => (
                  <FieldTime
                    {...field}
                    data-cy="pitching-time-start"
                    error={errors.pitchingTimeStart}
                    label="Start"
                    onChange={async (e) => {
                      setFieldTouched('pitchingTimeStart', true);
                      await setFieldValue('pitchingTimeStart', e.target.value);
                    }}
                    placeholder="HH:MM"
                    required
                    touched={touched.pitchingTimeStart}
                  />
                )}
              />
              <Field
                name="pitchingTimeEnd"
                render={({ field }) => (
                  <FieldTime
                    {...field}
                    data-cy="pitching-time-end"
                    error={errors.pitchingTimeEnd}
                    label="Finish"
                    onChange={async (e) => {
                      let value =
                        parseInt(values.pitchingTimeStart.replace(':', '')) >
                          parseInt(e.target.rawValue) && e.target.rawValue.length === 4
                          ? values.pitchingTimeStart
                          : e.target.value;
                      setFieldTouched('pitchingTimeEnd', true);
                      await setFieldValue('pitchingTimeEnd', value);
                    }}
                    placeholder="HH:MM"
                    required
                    style={{ marginRight: 0 }}
                    touched={touched.pitchingTimeEnd}
                  />
                )}
              />
            </Grid>
            <Field
              name="address"
              render={({ field }) => (
                <FieldText
                  {...field}
                  data-cy="place"
                  error={errors.address}
                  label="Place"
                  onChange={(e) => {
                    setFieldTouched('address', true);
                    setFieldValue('address', e.target.value);
                  }}
                  placeholder={placeholder.shortText('place')}
                  required
                  touched={touched.address}
                  type="long"
                />
              )}
            />
            <Button
              disabled={disabled}
              large
              onClick={(e) => this._handleToggleDialog(e)}
              type="button"
            >
              Submit
            </Button>
          </form>
        </SideDrawer>
      </>
    );
  }
}

Component.defaultProps = {
  handleSubmit: () => {},
  invalid: false,
  isLoading: false,
  submitting: false,
};

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  isLoading: PropTypes.bool,
  onRescheduleItem: PropTypes.func.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  submitting: PropTypes.bool,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
