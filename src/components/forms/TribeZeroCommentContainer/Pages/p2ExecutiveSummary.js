import React from 'react';
import { placeholder } from '../../../../constants/copywriting';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldEditor from '../../../elements/FieldEditor';
import FieldText from '../../../elements/FieldText';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { errors, touched, setFieldValue, setFieldTouched, handleAutosave, values } = this.props;

    return (
      <React.Fragment>
        <h3>Executive Summary</h3>
        <p className="sub-text">
          This page identify information about content of business idea proposals to be submitted.
        </p>

        <FieldEditor
          block
          data-cy="idea-background"
          disabled
          label="Background"
          placeholder={placeholder.longText('background that will be developed', 700)}
          required
          value={values.background}
        />

        <Field
          name="feedbackBackground"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="feedback-background"
              error={errors.feedbackBackground}
              info="This section identifies an evaluation of Tribe Zero regarding the submission of business idea proposals."
              label="Evaluation of Background"
              onChange={(e) => {
                setFieldTouched('feedbackBackground', true);
                setFieldValue('feedbackBackground', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(`evaluation of background`, 700)}
              required
              touched={touched.feedbackBackground}
              type="long"
            />
          )}
        />

        <FieldEditor
          block
          data-cy="idea-desc"
          disabled
          label="Description of Business Idea"
          placeholder={placeholder.longText('description of business idea', 700)}
          required
          value={values.desc}
        />

        <Field
          name="feedbackDesc"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="feedback-desc"
              error={errors.feedbackDesc}
              info="This section identifies an evaluation of Tribe Zero regarding the submission of business idea proposals."
              label="Evaluation of Description of Business Idea"
              onChange={(e) => {
                setFieldTouched('feedbackDesc', true);
                setFieldValue('feedbackDesc', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(`evaluation of description of business idea`, 700)}
              required
              touched={touched.feedbackDesc}
              type="long"
            />
          )}
        />

        <FieldEditor
          block
          data-cy="benefit-telkom"
          disabled
          label="Value Creation - Benefit for Telkom"
          placeholder={placeholder.longText('benefit for Telkom', 700)}
          required
          value={values.telkomBenefit}
        />

        <Field
          name="feedbackTelkomBenefit"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="feedback-telkom-benefit"
              error={errors.feedbackTelkomBenefit}
              info="This section identifies an evaluation of Tribe Zero regarding the submission of business idea proposals."
              label="Evaluation of Value Creation - Benefit for Telkom"
              onChange={(e) => {
                setFieldTouched('feedbackTelkomBenefit', true);
                setFieldValue('feedbackTelkomBenefit', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(
                `evaluation of value creation - benefit for Telkom`,
                700,
              )}
              required
              touched={touched.feedbackTelkomBenefit}
              type="long"
            />
          )}
        />

        <FieldEditor
          block
          data-cy="benefit-customer"
          disabled
          label="Value Creation - Benefit for Customer"
          placeholder={placeholder.longText('benefit for customer', 700)}
          required
          value={values.customerBenefit}
        />

        <Field
          name="feedbackCustomerBenefit"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="feedback-customer-benefit"
              error={errors.feedbackCustomerBenefit}
              info="This section identifies an evaluation of Tribe Zero regarding the submission of business idea proposals."
              label="Evaluation of Value Creation - Benefit for Customer"
              onChange={(e) => {
                setFieldTouched('feedbackCustomerBenefit', true);
                setFieldValue('feedbackCustomerBenefit', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(
                `evaluation of value creation - benefit for customer`,
                700,
              )}
              required
              touched={touched.feedbackCustomerBenefit}
              type="long"
            />
          )}
        />

        <FieldEditor
          block
          data-cy="value-capture"
          disabled
          label="Value Capture"
          placeholder={placeholder.longText('value capture', 700)}
          required
          value={values.valueCapture}
        />

        <Field
          name="feedbackValueCapture"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="feedback-value-capture"
              error={errors.feedbackValueCapture}
              info="This section identifies an evaluation of Tribe Zero regarding the submission of business idea proposals."
              label="Evaluation of Value Capture"
              onChange={(e) => {
                setFieldTouched('feedbackValueCapture', true);
                setFieldValue('feedbackValueCapture', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(`evaluation of value capture`, 700)}
              required
              touched={touched.feedbackValueCapture}
              type="long"
            />
          )}
        />
      </React.Fragment>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
