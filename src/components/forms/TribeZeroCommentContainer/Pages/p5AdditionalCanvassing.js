import React from 'react';
import { placeholder } from '../../../../constants/copywriting';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldText from '../../../elements/FieldText';
import FieldSelect from '../../../elements/FieldSelect';
import { optionalCanvassing } from '../../../../utils/dropdownList';
import Dropzone from '../../../elements/Dropzone';
import { Grid } from '@material-ui/core';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }

  _getInitialValues(values) {
    if (values.additionalCanvasName === 'Lean Canvas') {
      return {
        image: values.leanCanvasUrl,
      };
    }

    return {
      image: values.businessModelUrl,
    };
  }

  render() {
    const {
      errors,
      touched,
      setFieldTouched,
      setFieldValue,
      handleAutosave,
      values,
      onFilesAdded,
      onFilesRemoved,
    } = this.props;

    return (
      <React.Fragment>
        <h3>Canvassing of Product - Additional Canvassing</h3>{' '}
        <p className="sub-text">This page identify the explanation of product canvassing. </p>
        <FieldSelect
          dataCy="additional-canvassing"
          disabled
          label="Additional Canvassing"
          options={optionalCanvassing}
          placeholder={placeholder.select('canvassing')}
          required
          value={values.additionalCanvasName}
        />
        {values.additionalCanvasName !== '' && (
          <>
            <Grid container style={{ marginBottom: '1rem' }}>
              <Grid item lg={6} md={6} xs={12}>
                <Dropzone
                  dataCy="lean-canvas-image"
                  disabled
                  error={errors.leanCanvasUrl}
                  handleTouch={setFieldTouched}
                  image={values.leanCanvasUrl}
                  initialValues={{
                    bodyText: 'Click or drag image here (format jpg/png)',
                    captionText: '30 MB Maximum',
                  }}
                  isFailed={values.leanCanvas.isFailed}
                  isUploaded={values.leanCanvas.isUploaded}
                  label="Lean Canvas of Product"
                  name="leanCanvasUrl"
                  onFilesAdded={(files) =>
                    onFilesAdded(files, 'leanCanvasUrl', setFieldValue, values)
                  }
                  onFilesRemoved={(files) =>
                    onFilesRemoved(files, 'leanCanvasUrl', setFieldValue, values)
                  }
                  progress={values.leanCanvas.progress}
                  required
                  style={{
                    width: '26rem',
                    display: values.additionalCanvasName !== 'Lean Canvas' ? 'none' : 'flex',
                  }}
                  touched={touched.leanCanvasUrl}
                />
                <Dropzone
                  dataCy="buss-model-canvas"
                  disabled
                  error={errors.businessModelUrl}
                  handleTouch={setFieldTouched}
                  image={values.businessModelUrl}
                  initialValues={{
                    bodyText: 'Click or drag image here (format jpg/png)',
                    captionText: '30 MB Maximum',
                  }}
                  isFailed={values.businessModel.isFailed}
                  isUploaded={values.businessModel.isUploaded}
                  label="Business Model Canvas of Product"
                  name="businessModelUrl"
                  onFilesAdded={(files) =>
                    onFilesAdded(files, 'businessModelUrl', setFieldValue, values)
                  }
                  onFilesRemoved={(files) =>
                    onFilesRemoved(files, 'businessModelUrl', setFieldValue, values)
                  }
                  progress={values.businessModel.progress}
                  required
                  style={{
                    width: '26rem',
                    display: values.additionalCanvasName === 'Lean Canvas' ? 'none' : 'flex',
                  }}
                  touched={touched.businessModelUrl}
                />
              </Grid>
            </Grid>

            <FieldText
              block
              data-cy="explanation"
              disabled
              label="Explanation"
              placeholder={placeholder.longText('explanation', 700)}
              required
              type="long"
              value={values.additionalCanvasDesc}
            />

            <Field
              name="feedbackAdditionalCanvas"
              render={({ field }) => (
                <FieldText
                  {...field}
                  block
                  data-cy="feedback-additional-canvas"
                  error={errors.feedbackAdditionalCanvas}
                  info="This section identifies an evaluation of Tribe Zero regarding the submission of business idea proposals."
                  label="Evaluation of Additional Canvassing"
                  onChange={(e) => {
                    setFieldTouched('feedbackAdditionalCanvas', true);
                    setFieldValue('feedbackAdditionalCanvas', e.target.value);
                    handleAutosave;
                  }}
                  placeholder={placeholder.longText(`evaluation of additional canvassing`, 700)}
                  required
                  touched={touched.feedbackAdditionalCanvas}
                  type="long"
                />
              )}
            />
          </>
        )}
      </React.Fragment>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  onFilesAdded: PropTypes.func.isRequired,
  onFilesRemoved: PropTypes.func.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
