import React from 'react';
import Grid from '@material-ui/core/Grid';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldText from '../../../elements/FieldText';
import { placeholder } from '../../../../constants/copywriting';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.setBudgetTotal();
  }

  async setBudgetTotal() {
    const { values, setFieldValue } = this.props;
    let costTalent = isNaN(values.costTalent) || values.costTalent === '' ? 0 : values.costTalent;
    let costEnabler =
      isNaN(values.costEnabler) || values.costEnabler === '' ? 0 : values.costEnabler;
    let rewardForTalent =
      isNaN(values.rewardForTalent) || values.rewardForTalent === '' ? 0 : values.rewardForTalent;
    let development =
      isNaN(values.development) || values.development === '' ? 0 : values.development;
    let partnership =
      isNaN(values.partnership) || values.partnership === '' ? 0 : values.partnership;
    let operational =
      isNaN(values.operational) || values.operational === '' ? 0 : values.operational;
    let budgetTotal =
      costTalent + costEnabler + rewardForTalent + development + partnership + operational;
    await setFieldValue('budgetTotal', parseInt(budgetTotal));
  }

  render() {
    const { errors, touched, setFieldValue, setFieldTouched, handleAutosave, values } = this.props;

    return (
      <>
        <h3>Resource Submission - Proposed Budget</h3>
        <p className="sub-text">
          This page identify the information about budget submission of this business idea.
        </p>
        <h5 style={{ marginBottom: '0.5rem' }}>
          <strong>Talent Based</strong>
        </h5>
        <Grid className="field-group" container>
          <FieldText
            data-cy="cost-talent"
            disabled
            label="Cost Talent"
            placeholder="Rp."
            required
            style={{ width: '15rem' }}
            type="rupiah"
            value={values.costTalent}
          />
          <FieldText
            data-cy="cost-enabler"
            disabled
            label="Cost Enabler"
            placeholder="Rp."
            required
            style={{ width: '15rem' }}
            type="rupiah"
            value={values.costEnabler}
          />
          <FieldText
            data-cy="reward-talent"
            disabled
            label="Reward for Talent"
            placeholder="Rp."
            required
            style={{ width: '15rem' }}
            type="rupiah"
            value={values.rewardForTalent}
          />
          <FieldText
            data-cy="cost-development"
            disabled
            label="Development"
            placeholder="Rp."
            required
            style={{ width: '15rem' }}
            type="rupiah"
            value={values.development}
          />
        </Grid>

        <h5 style={{ marginBottom: '0.5rem' }}>
          <strong>Project Based</strong>
        </h5>
        <Grid className="field-group" container>
          <FieldText
            data-cy="cost-partnership"
            disabled
            label="Partnership"
            placeholder="Rp."
            required
            style={{ width: '15rem' }}
            type="rupiah"
            value={values.partnership}
          />
          <FieldText
            data-cy="cost-operational"
            disabled
            label="Operational"
            placeholder="Rp."
            required
            style={{ width: '15rem' }}
            type="rupiah"
            value={values.operational}
          />
        </Grid>
        <FieldText
          data-cy="cost-total"
          disabled
          label="Total Budget"
          max={13}
          placeholder="Rp."
          required
          style={{ width: '15rem' }}
          type="rupiah"
          value={values.budgetTotal}
        />

        <Field
          name="feedbackBudget"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="feedback-budget"
              error={errors.feedbackBudget}
              info="This section identifies an evaluation of Tribe Zero regarding the submission of business idea proposals."
              label="Evaluation of Proposed Budget"
              onChange={(e) => {
                setFieldTouched('feedbackBudget', true);
                setFieldValue('feedbackBudget', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(`evaluation of proposed budget`, 700)}
              required
              touched={touched.feedbackBudget}
              type="long"
            />
          )}
        />
      </>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
