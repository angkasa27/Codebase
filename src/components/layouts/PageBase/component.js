import React from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { ICONS, COUNT_PAGE } from '../../../configs';
import Drawer from '../../elements/Drawer';
import styles from './styles.css';

export default class Component extends React.Component {
  constructor() {
    super();
    if (localStorage.count_page === '1') {
      this.state = {
        openSnackbar: true,
      };
    }
    else {
      this.state = {
        openSnackbar: false,
      };
    }
  }
  
  _handleClickCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return localStorage.setItem(COUNT_PAGE, 2);
    }
    this.setState({ openSnackbar: false });
    localStorage.setItem(COUNT_PAGE, 2);
  }

  _renderSnackbar() {
    return (
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        autoHideDuration={6000}
        className={styles['snackbar']}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        onClose={this._handleClickCloseSnackbar}
        open={this.state.openSnackbar}
      >
        <SnackbarContent
          action={[
            <IconButton
              aria-label="Close"
              className={styles['snackbar-button']}
              color="inherit"
              key="close"
              onClick={this._handleClickCloseSnackbar}
            >
              <CloseIcon className={styles['snackbar-icon']} />
            </IconButton>,
          ]}
          aria-describedby="client-snackbar"
          className={styles['snackbar-content']}
          message={
            <span className={styles['snackbar-message']} id="client-snackbar">
              <img className={styles['snackbar-icon']} src={ICONS.ICON_SUCCESS} />
              You have successfully signed in!
            </span>
          }
        />
      </Snackbar>
    );
  }

  render() {
    const { children } = this.props;

    return (
      <>
        <Drawer />
        <main className={styles.content}>
          {children}
          {this._renderSnackbar()}
        </main>
      </>
    );
  }
}

Component.defaultProps = {
  children: {},
};

Component.propTypes = {
  children: PropTypes.node,
};
