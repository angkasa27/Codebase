/* eslint-disable max-len */
import React from 'react';
import PropTypes from 'prop-types';
import FieldText from '../../elements/FieldText';
import { Grid } from '@material-ui/core';
import { Field, getIn } from 'formik';
import { placeholder } from '../../../constants/copywriting';

export default class Component extends React.Component {
  render() {
    const { field, disabled, errors, touched, squadIdx, talIdx, setFieldValue } = this.props;

    return (
      <>
        {field.map((tal, idx) => (
          <Grid container key={idx} style={{ paddingLeft: '2rem' }}>
            <div style={{ transform: `translate(0, 2.4rem)`, marginRight: '1rem' }}>
              <h5>{idx + 1}.</h5>
            </div>
            <Field
              name={`squadRequest[${squadIdx}].talentRequest[${talIdx}].talentExisting[${idx}].name`}
              render={({ field }) => (
                <FieldText
                  {...field}
                  disabled={disabled}
                  error={getIn(
                    errors,
                    `squadRequest[${squadIdx}].talentRequest[${talIdx}].talentExisting[${idx}].name`,
                  )}
                  label="Name of Existing Talent"
                  onChange={(e) => {
                    setFieldValue(
                      `squadRequest[${squadIdx}].talentRequest[${talIdx}].talentExisting[${idx}].name`,
                      e.target.value,
                    );
                  }}
                  placeholder={placeholder.shortText('name of existing talent')}
                  required
                  small
                  touched={getIn(
                    touched,
                    `squadRequest[${squadIdx}].talentRequest[${talIdx}].talentExisting[${idx}].name`,
                  )}
                />
              )}
            />
            <Field
              name={`squadRequest[${squadIdx}].talentRequest[${talIdx}].talentExisting[${idx}].email`}
              render={({ field }) => (
                <FieldText
                  {...field}
                  disabled={disabled}
                  error={getIn(
                    errors,
                    `squadRequest[${squadIdx}].talentRequest[${talIdx}].talentExisting[${idx}].email`,
                  )}
                  label="Email Address"
                  onChange={(e) => {
                    setFieldValue(
                      `squadRequest[${squadIdx}].talentRequest[${talIdx}].talentExisting[${idx}].email`,
                      e.target.value,
                    );
                  }}
                  placeholder={placeholder.shortText('email address')}
                  required
                  small
                  touched={getIn(
                    touched,
                    `squadRequest[${squadIdx}].talentRequest[${talIdx}].talentExisting[${idx}].email`,
                  )}
                />
              )}
            />
          </Grid>
        ))}
      </>
    );
  }
}

Component.defaultProps = {
  className: '',
  disabled: false,
  squadIdx: 0,
  talIdx: 0,
};

Component.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
  errors: PropTypes.object.isRequired,
  field: PropTypes.array.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  squadIdx: PropTypes.number,
  talIdx: PropTypes.number,
  touched: PropTypes.object.isRequired,
};
