import React from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import Pagination from '../../elements/Pagination';
import Tooltip from '../../elements/Tooltip';
import { ICONS } from './../../../configs';
import { changeDate } from '../../../utils/dateFormat';
import styles from './styles.css';

export default class Component extends React.Component {
  _handleChangeSorting = (e) => {
    const { onChangeSort } = this.props;
    const field = e.target.getAttribute('index');
    const query = queryString.parse(location.search);

    if (query.sort) {
      let value = undefined;
      const char = query.sort.split('-');

      if (query.sort.charAt(0) === '-') {
        value = char[1] === field ? undefined : field;
      } else {
        value = char[0] === field ? `-${field}` : field;
      }

      onChangeSort({ name: 'sort', value });
    } else {
      onChangeSort({ name: 'sort', value: field });
    }
  }

  _handleIconSort(item) {
    const query = queryString.parse(location.search);
    let icon = '';

    if (query.sort) {
      const char = query.sort.split('-');
      if (query.sort.charAt(0) === '-') {
        icon = char[1] === item ? ICONS.SORT_DOWN : ICONS.ICON_SORT;
      } else {
        icon = char[0] === item ? ICONS.SORT_UP : ICONS.ICON_SORT;
      }
    } else {
      if(item === 'startDate') {
        icon = ICONS.SORT_DOWN;
      } else {
        icon = ICONS.ICON_SORT;
      }
    }

    return (
      <img
        className={styles['sort-icon']}
        index={item}
        onClick={this._handleChangeSorting}
        src={icon} />
    );
  }

  _renderHeader() {
    return (
      <tr>
        <th>
          <span className={styles['span-title']}>
            <h5 className={styles.title}><strong>Project Name</strong></h5>
            {this._handleIconSort('projectName')}
          </span>
        </th>
        <th>
          <span className={styles['span-title']}>
            <h5 className={styles.title}><strong>Product Name</strong></h5>
            {this._handleIconSort('productName')}
          </span>
        </th>
        <th>
          <span className={styles['span-title']}>
            <h5 className={styles.title}><strong>Squad Name</strong></h5>
            {this._handleIconSort('squadName')}
          </span>
        </th>
        <th className={styles['th-level']}>
          <span className={styles['span-title']}>
            <h5 className={styles['title-member']}><strong>Member</strong></h5>
            {this._handleIconSort('memberCount')}
          </span>
        </th>
        <th className={styles['th-level']}>
          <span className={styles['span-title']}>
            <h5 className={styles['title-achievement']}><strong>OMTM Achievement</strong></h5>
            {this._handleIconSort('omtmAchievement')}
          </span>
        </th>
        <th className={styles['th-level']}>
          <span className={styles['span-title']}>
            <h5 className={styles['title-budget']}><strong>Budget Realization</strong></h5>
            {this._handleIconSort('budget')}
          </span>
        </th>
        <th className={styles['th-level']}>
          <span className={styles['span-title']}>
            <h5 className={styles['title-date']}><strong>Start Date</strong></h5>
            {this._handleIconSort('startDate')}
          </span>
        </th>
        <th className={styles['th-level']}>
          <h5 className={styles['title-action']}><strong>Action</strong></h5>
        </th>
      </tr>
    );
  }

  _renderListProject() {
    const { dataProject, meta } = this.props;

    return (
      <>
        <table className={styles.table}>
          <thead>
            {this._renderHeader()}
          </thead>
          <tbody className={styles.tbody}>
            {dataProject.map((row, idx) => (
              <tr key={idx}>
                <td>
                  <Tooltip placement="bottom-start" title={row.projectName} ><span className={styles['wrap-text']}>{row.projectName}</span></Tooltip>
                </td>
                <td>
                  <Tooltip placement="bottom-start" title={row.productName} ><span className={styles['wrap-text']}>{row.productName}</span></Tooltip>
                </td>
                <td>
                  <Tooltip placement="bottom-start" title={row.squadName} ><span className={styles['wrap-text']}>{row.squadName}</span></Tooltip>
                </td>
                <td className={styles['td-level']}>{row.memberCount}</td>
                <td className={styles['td-level']}>{row.omtmAchievement}%</td>
                <td className={styles['td-level']}>{row.budget}%</td>
                <td className={styles['td-level']}>{changeDate(row.startDate)}</td>
                <td className={styles['td-level']}><a className={styles['td-action']} href={`/project/details/${row.squadId}`} >Details</a></td>
              </tr>
            ))}
          </tbody>
        </table>
        {meta ? <Pagination className={styles.pagination} meta={meta} onChangeUrl={this.props.onChangeUrl} /> : ''}
      </>
    );
  }

  _renderNoData() {
    return(
      <main className={styles['empty-container']}>
        <div>
          <header className={styles['empty-image']}>
            <img src={ICONS.ICON_NO_DATA} />
          </header>
          <h4 className={styles['empty-text']}>Sorry, no results were found.</h4>
        </div>
      </main>
    );
  }

  render() {
    const { dataProject, isLoading } = this.props;

    if (!isLoading) {
      return (
        <>
          {dataProject.length === 0 ? this._renderNoData() : this._renderListProject()}
        </>
      );
    } else return null;
  }
}

Component.defaultProps = {
  dataProject: [],
  isLoading: false,
  match: {},
  meta: {},
  onChangeSort: {},
  onChangeUrl: {},
};

Component.propTypes = {
  dataProject: PropTypes.array,
  isLoading: PropTypes.bool,
  match: PropTypes.object,
  meta: PropTypes.object,
  onChangeSort: PropTypes.func,
  onChangeUrl: PropTypes.func,
};
