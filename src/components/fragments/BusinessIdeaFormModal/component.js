import React from 'react';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '../../elements/Dialog';
import Button from '../../elements/Button';
import PropTypes from 'prop-types';

export default class Component extends React.Component {
  _handleClose = (e) => {
    e.preventDefault();
    this.props.onHandleClose();
  };

  _handleSubmit = (e) => {
    e.preventDefault();
    this.props.onHandleSubmit();
  };

  render() {
    return (
      <>
        <Dialog
          aria-describedby="alert-dialog-description"
          aria-labelledby="alert-dialog-title"
          onClose={this._handleClose}
          open={this.props.isOpen}
        >
          <DialogTitle id="alert-dialog-title">Do you want to submit the proposal?</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              This process can not be undone
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus ghost onClick={this._handleClose} type="button">
              Cancel
            </Button>
            <Button autoFocus onClick={this._handleSubmit} type="button">
              Submit
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}

Component.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onHandleClose: PropTypes.func.isRequired,
  onHandleSubmit: PropTypes.func.isRequired,
};
