import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import Text from '../Text';
import Grid from '@material-ui/core/Grid';

const StyledCircularProgress = withStyles({
  indeterminate: {
    color: '#289f97'
  }
})(CircularProgress);

const Fetching = () => (
  <Grid
    alignItems="center"
    container
    direction="column"
    justify="center"
    style={{ margin: '6.5rem auto' }}
  >
    <Grid item style={{ marginBottom: '1rem' }}>
      <StyledCircularProgress size={32} style={{ verticalAlign: 'middle' }} />
    </Grid>
    <Grid item>
      <Text>Please wait, data fetching in progress…</Text>
    </Grid>
  </Grid>
);

export default Fetching;
