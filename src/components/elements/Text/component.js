import React from 'react';
import PropTypes from 'prop-types';

export default class Component extends React.Component {
  render() {
    const { children, classes, className, variant, ...rest } = this.props;
    const customVariant = variant ? `${classes.body2} ${classes[variant]}` : classes.body2;
    const customClass = className ? `${customVariant} ${className}` : customVariant;

    return (
      <p className={customClass} {...rest}>
        {children}
      </p>
    );
  }
}

Component.defaultProps = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.number,
    PropTypes.string,
  ]),
  classes: PropTypes.object,
  className: PropTypes.string,
  variant: PropTypes.string,
};

Component.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.number,
    PropTypes.string,
  ]),
  classes: PropTypes.object,
  className: PropTypes.string,
  variant: PropTypes.string,
};
