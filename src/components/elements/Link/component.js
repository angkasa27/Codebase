import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';

export default class Component extends React.Component {
  render() {
    const { children, className, path } = this.props;

    return (
      <a className={`${className} ${styles['link']}`} href={path}>
        {children}
      </a>
    );
  }
}

Component.defaultProps = {
  children: null,
  className: '',
  path: '',
};

Component.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  path: PropTypes.string,
};
