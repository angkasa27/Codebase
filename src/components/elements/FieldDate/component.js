import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Popover from '@material-ui/core/Popover';
import DateRangePicker from 'react-daterange-picker';
import Moment from 'moment';
import { extendMoment } from 'moment-range';
import ErrorMessage from '../ErrorMessage';
import Grid from '@material-ui/core/Grid';
import './styles.scss';

const moment = extendMoment(Moment);

const StyledPopover = withStyles({
  paper: {
    boxShadow: '0 0 1px 0 rgba(72, 122, 157, 0.47)',
    borderRadius: 5,
    marginTop: 8,
  },
})(Popover);

export default function FieldDate(props) {
  const {
    value,
    onChange,
    dateFormat,
    useFormattedValue,
    className,
    field,
    label,
    touched,
    error,
    style,
    isRange,
  } = props;

  const [anchorEl, setAnchorEl] = useState();
  const [rangeValue, setRangeValue] = useState();
  const [singleValue, setSingleValue] = useState();
  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    if (value) {
      if (isRange) {
        let range = {};
        if (useFormattedValue) {
          const rawValue = {
            start: moment(value.startDate, dateFormat),
            end: moment(value.endDate, dateFormat),
          };
          range = moment.range(rawValue.start, rawValue.end);
        } else {
          range = moment.range(value);
        }
        const { start, end } = range;
        setInputValue(`${start.format(dateFormat)} ~ ${end.format(dateFormat)}`);
        setRangeValue(range);
      } else {
        let _value = '';
        if (useFormattedValue) {
          _value = moment(value, dateFormat);
        } else {
          _value = moment(value);
        }
        setInputValue(_value.format(dateFormat));
        setSingleValue(_value);
      }
    } else {
      setInputValue('');
      setRangeValue(null);
      setSingleValue(null);
    }
  }, [value]);

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function _getClass() {
    let className = 'cdx-input';
    if (props.error && props.touched) {
      className = `${className} error-input`;
    }
    if (props.small) {
      className = `${className} small`;
    }
    if (props.block) {
      className = `${className} block`;
    } else if (props.long) {
      className = `${className} long`;
    }

    return className;
  }

  function _getMainClass() {
    let className = 'cdx-datepicker';
    if (props.block) {
      className = `${className} 'block'`;
    } else if (props.long) {
      className = `${className} 'long'`;
    }

    return className;
  }

  const open = Boolean(anchorEl);

  function handleSelect(value) {
    let event = '';
    if (isRange) {
      const { start, end } = value;

      setInputValue(`${start.format(dateFormat)} ~ ${end.format(dateFormat)}`);

      setRangeValue(value);

      event = {
        startDate: useFormattedValue ? start.format(dateFormat) : start._d,
        endDate: useFormattedValue ? end.format(dateFormat) : end._d,
      };
    } else {
      setInputValue(value.format(dateFormat));
      setSingleValue(value);
      event = useFormattedValue ? value.format(dateFormat) : value;
    }

    onChange(event);

    handleClose();
  }

  return (
    <>
      <div className={`${_getMainClass()} ${className}`} direction="column" style={style}>
        <Grid align="end" className="label" container justify="space-between">
          {label !== '' ? <p>{label}</p> : <p style={{ color: '#00000000' }}>.</p>}
        </Grid>
        <input
          {...props}
          {...field}
          className={_getClass()}
          onClick={handleClick}
          type="text"
          value={inputValue}
        />
        {touched && error && <ErrorMessage>{error}</ErrorMessage>}
        <StyledPopover
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          id="date-range-pop-over"
          onClose={handleClose}
          open={open}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
        >
          <div className="container-popover">
            <DateRangePicker
              firstOfWeek={1}
              numberOfCalendars={isRange ? 2 : 1}
              onSelect={handleSelect}
              selectionType={isRange ? 'range' : 'single'}
              singleDateRange={!isRange}
              value={isRange ? rangeValue : singleValue}
            />
          </div>
        </StyledPopover>
      </div>
    </>
  );
}

FieldDate.defaultProps = {
  block: false,
  className: '',
  dateFormat: 'DD-MM-YYYY',
  error: '',
  field: {},
  isRange: false,
  label: '',
  long: false,
  small: false,
  style: {},
  touched: false,
  useFormattedValue: false,
  value: '',
};

FieldDate.propTypes = {
  block: PropTypes.bool,
  className: PropTypes.string,
  dateFormat: PropTypes.string,
  error: PropTypes.string,
  field: PropTypes.object,
  isRange: PropTypes.bool,
  label: PropTypes.string,
  long: PropTypes.bool,
  onChange: PropTypes.bool.isRequired,
  small: PropTypes.bool,
  style: PropTypes.object,
  touched: PropTypes.bool,
  useFormattedValue: PropTypes.bool,
  value: PropTypes.string,
};
