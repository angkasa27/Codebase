import React from 'react';
import renderer from 'react-test-renderer';
import Component from '../component';
import { ICONS } from '../../../../configs';

describe('Button', () => {
  it('renders correctly with messages', () => {
    const tree = renderer.create(<Component>Hellow World</Component>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly using ghost style', () => {
    const tree = renderer.create(<Component ghost>Hellow World</Component>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly using flat style', () => {
    const tree = renderer.create(<Component flat>Hellow World</Component>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly using delete style', () => {
    const tree = renderer.create(<Component delete>Hellow World</Component>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly with small size', () => {
    const tree = renderer.create(<Component small>Hellow World</Component>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly with large size', () => {
    const tree = renderer.create(<Component large>Hellow World</Component>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly with maximum width', () => {
    const tree = renderer.create(<Component block>Hellow World</Component>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly with loading', () => {
    const tree = renderer.create(<Component isloading>Hellow World</Component>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly with circle style and icon', () => {
    const tree = renderer.create(<Component circle icon={ICONS.MINUS_ICON} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
