import { ACTIONS } from '../../../constants';

const initData = {
  isOpen: false,
  variant: '',
  message: ''
};

export function snackbar(state = initData, action) {
  switch (action.type) {
    case ACTIONS.OPEN_SNACKBAR:
      return {
        isOpen: true,
        variant: action.variant,
        message: action.message
      };
    default:
      return state;
  }
}
