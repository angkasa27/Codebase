import React from 'react';
import { shallow } from 'enzyme';
import Component from '../component';
import { Formik, Field } from 'formik';

describe('Field Editor', () => {
  it('renders correctly', () => {
    const actions = {
      onBlur: jest.fn(),
      onChange: jest.fn()
    };

    const wrapper = shallow(
      <Formik>
        <Field {...actions} block component={Component} required={false} />
      </Formik>
    );
    expect(wrapper).toBeCalled;
  });

  it('renders with error', () => {
    const actions = {
      onBlur: jest.fn(),
      onChange: jest.fn()
    };

    const wrapper = shallow(
      <Component {...actions} block error="this is error message" required touched />
    );
    expect(wrapper).toBeCalled;
  });

  it('update Value', () => {
    const actions = {
      onBlur: jest.fn(),
      onChange: jest.fn()
    };

    let value = '<p>This is Paragraph Value</p>';

    const wrapper = shallow(<Component {...actions} label="Text Editor" required value={value} />);
    const boldButton = wrapper.find('button').at(0);
    const italicButton = wrapper.find('button').at(1);
    const underlineButton = wrapper.find('button').at(2);

    boldButton.simulate('click');
    italicButton.simulate('click');
    underlineButton.simulate('click');

    value = '<p>The <em>Value</em> has <u>been</u> <strong>updated</strong></p>';

    expect(wrapper).toBeCalled;
  });
});
