import React from 'react';
import PropTypes from 'prop-types';
import DialogActions from '@material-ui/core/DialogActions';
import styles from '../styles.css';

export default class Component extends React.Component {
  render() {
    return (
      <DialogActions {...this.props} className={styles['modal-action']}>
        {this.props.children}
      </DialogActions>
    );
  }
}

Component.defaultProps = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.number,
    PropTypes.string,
  ]),
};

Component.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.number,
    PropTypes.string,
  ]),
};
